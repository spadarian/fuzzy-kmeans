use crate::{run_optim, Distance};
use data::IRIS;
use ndarray::arr2;

mod data;

#[test]
#[allow(non_snake_case)]
fn test_something() {
    let data = arr2(&IRIS);
    let nclass = 3;
    let phi = 1.5;
    let alpha = 0.0000001;
    let disttype = Distance::Mahalanobis;
    let maxiter = 300;
    let toldiff = 0.001;
    let exp_eg = 1.0 / (1.0 + nclass as f64);

    let exp_diff = run_optim(data, nclass, phi, alpha, disttype, maxiter, toldiff, exp_eg);
    assert!(0.25 - exp_diff < 0.001);
}
