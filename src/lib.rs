use crate::{
    distance::calc_dist,
    utils::{normalise_cols, update_centroids},
};
pub use crate::{distance::Distance, utils::calc_membership};
use ndarray::{concatenate, Array, Array2, Axis};
use ndarray_linalg::solve::Inverse;
use ndarray_rand::rand_distr::Normal;
use ndarray_rand::RandomExt;
use ndarray_stats::CorrelationExt;

mod distance;
#[cfg(test)]
mod tests;
mod utils;

#[allow(non_snake_case)]
pub fn run(
    data: Array2<f64>,
    nclass: usize,
    phi: f64,
    alpha: f64,
    disttype: Distance,
    maxiter: usize,
    toldiff: f64,
) -> (Array2<f64>, Array2<f64>, Array2<f64>) {
    let (U, Ue, centroids, W) = run_base(data, nclass, phi, alpha, disttype, maxiter, toldiff);
    let U_end = concatenate![Axis(1), U, Ue];
    (U_end, centroids, W)
}

#[allow(non_snake_case)]
#[allow(clippy::too_many_arguments)]
pub fn run_optim(
    data: Array2<f64>,
    nclass: usize,
    phi: f64,
    alpha: f64,
    disttype: Distance,
    maxiter: usize,
    toldiff: f64,
    exp_eg: f64,
) -> f64 {
    let (_, Ue, _, _) = run_base(data, nclass, phi, alpha, disttype, maxiter, toldiff);

    let Ue_mean = Ue.mean().unwrap();
    (Ue_mean - exp_eg).abs()
}

#[allow(non_snake_case)]
fn run_base(
    data: Array2<f64>,
    nclass: usize,
    phi: f64,
    alpha: f64,
    disttype: Distance,
    maxiter: usize,
    toldiff: f64,
) -> (Array2<f64>, Array2<f64>, Array2<f64>, Array2<f64>) {
    let ndata = data.shape()[0];
    let ndim = data.shape()[1];

    let W = match disttype {
        Distance::Euclidean => Array::eye(ndim),
        Distance::Diagonal => Array::eye(ndim) * data.t().cov(1.0).unwrap(),
        Distance::Mahalanobis => data.t().cov(1.0).unwrap().inv().unwrap(),
    };

    let mut dist = Array2::<f64>::zeros((ndata, nclass));
    let mut U = Array::random((ndata, nclass), Normal::<f64>::new(0.5, 0.01).unwrap());
    U = normalise_cols(U).mapv(f64::abs);

    let Ue = 1.0
        - U.sum_axis(Axis(1))
            .mapv(f64::abs)
            .into_shape((U.shape()[0], 1))
            .unwrap();
    let mut uphi = U.mapv(|v| v.powf(phi));
    let mut uephi = Ue.mapv(|v| v.powf(phi));
    let a1 = (1.0 - alpha) / alpha;

    // Initialice
    let c1 = uphi.t().dot(&data);
    let t1 = uphi
        .sum_axis(Axis(0))
        .into_shape((uphi.shape()[1], 1))
        .unwrap();
    let mut centroids = c1 / t1;

    calc_dist(&mut dist, &data, &centroids, &W, &disttype);

    let mut U_old: Array2<f64>;
    let mut obj = 0.0;
    let mut obj_old: f64;

    for _ in 0..maxiter {
        update_centroids(&mut centroids, &uphi, &uephi, a1, &dist, &data);
        calc_dist(&mut dist, &data, &centroids, &W, &disttype);
        U_old = U.clone();
        obj_old = obj;

        // Calculate new membership matrix
        let (U, Ue) = calc_membership(&dist, phi, a1);
        uphi = U.mapv(|v| v.powf(phi));
        uephi = Ue.mapv(|v| v.powf(phi));

        // Calculate obj function
        let o1 = dist.mapv(|v| v.powi(2)) * &uphi;
        let d2 = dist.mapv(|v| v.powi(-2));
        let o2 = &uephi * d2.sum_axis(Axis(1)).into_shape((ndata, 1)).unwrap();
        obj = alpha * o1.sum() + (1.0 - alpha) * o2.sum();

        // Check for convergence
        let diff = obj_old - obj;
        let diffU = (&U - &U_old).mapv(f64::abs);
        let Udiff = diffU.sum();
        if (diff < toldiff) && (Udiff < toldiff) {
            break;
        }
    }

    update_centroids(&mut centroids, &uphi, &uephi, a1, &dist, &data);
    (U, Ue, centroids, W)
}
