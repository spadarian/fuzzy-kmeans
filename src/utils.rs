use ndarray::{s, Array2, Axis};

pub(crate) fn normalise_cols(arr: Array2<f64>) -> Array2<f64> {
    let colsum = arr
        .sum_axis(Axis(1))
        .into_shape((arr.shape()[0], 1))
        .unwrap();
    arr / colsum
}

pub(crate) fn update_centroids(
    centroids: &mut Array2<f64>,
    uphi: &Array2<f64>,
    uephi: &Array2<f64>,
    a1: f64,
    distance: &Array2<f64>,
    data: &Array2<f64>,
) {
    let ufi = distance.mapv(|v| v.powi(-4) * a1) * uephi;
    let c1 = ufi.t().dot(data);
    let t1 = ufi
        .sum_axis(Axis(0))
        .into_shape((uphi.shape()[1], 1))
        .unwrap();
    centroids.slice_mut(s![.., ..]).assign(&(c1 / t1));
}

#[allow(non_snake_case)]
pub fn calc_membership(distance: &Array2<f64>, phi: f64, a1: f64) -> (Array2<f64>, Array2<f64>) {
    let npoints = distance.shape()[0];
    let tmp = distance.mapv(|v| v.powf(2.0 / (phi - 1.0)));
    let tmp2 = distance.mapv(|v| v.powi(-2));
    let s2 = tmp2
        .sum_axis(Axis(1))
        .mapv(|v| (v * a1).powf(-1.0 / (phi - 1.0)))
        .into_shape((npoints, 1))
        .unwrap();

    let t1 = tmp.sum_axis(Axis(1)).into_shape((npoints, 1)).unwrap();
    let t2 = t1 + s2;
    let U = tmp / t2;
    let Ue = 1.0 - U.sum_axis(Axis(1)).into_shape((npoints, 1)).unwrap();
    (U.mapv(f64::abs), Ue.mapv(f64::abs))
}
