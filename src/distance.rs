use ndarray::{s, Array2, Axis};

pub enum Distance {
    Euclidean,
    Diagonal,
    Mahalanobis,
}

fn euclidean(dist: &mut Array2<f64>, data: &Array2<f64>, centroids: &Array2<f64>) {
    for (i, row) in centroids.outer_iter().enumerate() {
        let r = row.into_shape((1, data.shape()[1])).unwrap();
        let d = (data - &r)
            .mapv(|v| v.powi(2))
            .sum_axis(Axis(1))
            .mapv(f64::sqrt);
        dist.slice_mut(s![.., i]).assign(&d);
    }
}

#[allow(non_snake_case)]
fn mahalanobis(
    dist: &mut Array2<f64>,
    data: &Array2<f64>,
    centroids: &Array2<f64>,
    W: &Array2<f64>,
) {
    for (i, row) in centroids.outer_iter().enumerate() {
        let r = row.into_shape((1, data.shape()[1])).unwrap();
        let dc = data - &r;
        let d = (dc.dot(W) * dc).sum_axis(Axis(1));
        dist.slice_mut(s![.., i]).assign(&d);
    }
}

#[allow(non_snake_case)]
pub(crate) fn calc_dist(
    dist: &mut Array2<f64>,
    data: &Array2<f64>,
    centroids: &Array2<f64>,
    W: &Array2<f64>,
    distance: &Distance,
) {
    match distance {
        Distance::Euclidean => euclidean(dist, data, centroids),
        Distance::Diagonal | Distance::Mahalanobis => mahalanobis(dist, data, centroids, W),
    }
}
