# Fuzzy k-means with extragrades

This is the core library containing the functions required to run the fuzzy k-means with extragrades algorithm.

This clustering algorithm is a modification of (fuzzy) k-means in order to extend its usability.

- K-means: each observation belongs to a single cluster.
- Fuzzy k-means: each observation has a membership to each cluster. Think about an observation positioned exactly between 2 centroids. In that case the memberships to each cluster would be `0.5` (memberships add up to `1`).
- Fuzzy k-means with extragrades: The algorithm has an extra membership class that does not correspond to any of the clusters. Any observation that is too far from the centroids will be marked as belonging to such class. You can think about those observations as outliers.

### Related crates

There are other crates (not yet implemented) that will use this library:

- Library using [PyO3](https://github.com/PyO3/pyo3) to call functions from Python
- Rust CLI to run the complete optimisation process
